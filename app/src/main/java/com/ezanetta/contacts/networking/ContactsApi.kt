package com.ezanetta.contacts.networking

import com.ezanetta.contacts.domain.model.Contact
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface ContactsApi {

    @GET("/contacts")
    fun getContacts(): Call<List<Contact>>

    @GET("/contacts/{id}")
    fun getContact(@Path("id") id: String): Call<Contact>
}