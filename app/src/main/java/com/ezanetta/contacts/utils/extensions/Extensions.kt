package com.ezanetta.contacts.utils.extensions

import android.net.ConnectivityManager
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.ezanetta.contacts.R
import java.text.SimpleDateFormat
import java.util.*

fun ImageView.loadUrl(url: String?, placeholder: Int = R.drawable.contact) {
    Glide.with(this.context)
            .load(url)
            .placeholder(placeholder)
            .crossFade()
            .error(placeholder)
            .into(this)
}

fun String.toDateWithFormat(currentFormat: String, newFormat: String): String {
    val simpleFormat = SimpleDateFormat(currentFormat, Locale.getDefault())
    val newSimpleFormat = SimpleDateFormat(newFormat, Locale.getDefault())
    return newSimpleFormat.format(simpleFormat.parse(this))
}

fun ConnectivityManager.isNetworkAvailable(): Boolean {
    val networkInfo = this.activeNetworkInfo
    var isAvailable = false

    if (networkInfo != null && networkInfo.isConnected) {
        isAvailable = true
    }

    return isAvailable
}