package com.ezanetta.contacts.utils

val LIST_STATE_KEY: String = "list_state_key"
val CONTACTS_KEY: String = "contacts_key"
val CONTACT_KEY: String = "contact_key"
val CONTACT_ARG_KEY = "contact_arg_key"