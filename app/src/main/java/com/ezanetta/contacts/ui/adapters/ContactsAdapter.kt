package com.ezanetta.contacts.ui.adapters

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.ezanetta.contacts.R
import com.ezanetta.contacts.domain.model.Contact

class ContactsAdapter(val contacts: List<Contact>,
                      val listener: (Contact) -> Unit) : RecyclerView.Adapter<ContactViewHolder>() {

    override fun getItemCount(): Int = contacts.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_contact, parent, false)

        return ContactViewHolder(view)
    }

    override fun onBindViewHolder(holder: ContactViewHolder, position: Int) {
        with(contacts[position]) {
            holder.bindContact(this)
            holder.itemView.setOnClickListener { listener(this) }
        }
    }
}