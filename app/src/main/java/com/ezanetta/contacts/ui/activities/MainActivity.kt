package com.ezanetta.contacts.ui.activities

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.view.View
import com.ezanetta.contacts.R
import com.ezanetta.contacts.ui.fragments.ContactsFragment

class MainActivity : AppCompatActivity() {

    private lateinit var toolbar: Toolbar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        toolbar = findViewById(R.id.toolbar) as Toolbar
        setSupportActionBar(toolbar)

        setInitialFragment(savedInstanceState)
    }

    private fun setInitialFragment(savedInstanceState: Bundle?) {
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .add(R.id.container, ContactsFragment.newInstance(),
                            ContactsFragment::class.java.simpleName)
                    .commit()
        }
    }

    fun goToFragment(fragment: Fragment, tag: String) {
        val transaction = supportFragmentManager.beginTransaction()
        transaction.addToBackStack(tag)
        transaction.add(R.id.container, fragment, tag)
        transaction.commit()
    }

    fun setVisibilityToolbar(visible: Int) {
        toolbar.visibility = visible
    }

    fun restoreToolbar() {
        setSupportActionBar(toolbar)
        setVisibilityToolbar(View.VISIBLE)
    }
}
