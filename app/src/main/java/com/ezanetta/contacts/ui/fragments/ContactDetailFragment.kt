package com.ezanetta.contacts.ui.fragments


import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.ezanetta.contacts.R
import com.ezanetta.contacts.domain.dao.ContactsDao
import com.ezanetta.contacts.domain.model.Address
import com.ezanetta.contacts.domain.model.Contact
import com.ezanetta.contacts.domain.model.Phone
import com.ezanetta.contacts.domain.model.PhoneNumberType
import com.ezanetta.contacts.utils.CONTACT_ARG_KEY
import com.ezanetta.contacts.utils.CONTACT_KEY
import com.ezanetta.contacts.utils.extensions.isNetworkAvailable
import com.ezanetta.contacts.utils.extensions.loadUrl
import com.ezanetta.contacts.utils.extensions.toDateWithFormat


class ContactDetailFragment : BaseFragment(), ContactsDao.ContactDaoActions, BaseFragment.BaseActions {

    private lateinit var image: ImageView
    private lateinit var phoneHome: TextView
    private lateinit var phoneCellphone: TextView
    private lateinit var phoneOffice: TextView
    private lateinit var birthDay: TextView
    private lateinit var addressHome: TextView
    private lateinit var addressWork: TextView
    private lateinit var createdAt: TextView
    private lateinit var contactsDao: ContactsDao
    private lateinit var toolBar: Toolbar
    private lateinit var contact: Contact
    private lateinit var coordinator: CoordinatorLayout
    private lateinit var loading: ProgressBar
    private lateinit var phonesLabel: TextView
    private lateinit var addressesLabel: TextView
    val TAG: String = ContactDetailFragment::class.java.simpleName

    override fun onSuccess(contact: Contact) {
        this.contact = contact
        setupContact(contact)
    }

    override fun onError(throwable: Throwable) {
        Log.d(TAG, "Error ${throwable.localizedMessage}")
    }

    override fun onRetry() {
        getContact()
    }

    override fun getLayout(): Int = R.layout.fragment_contact_detail

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {
        val view = super.onCreateView(inflater, container, savedInstanceState)
        baseActions = this
        setHasOptionsMenu(true)

        with(view) {
            image = findViewById(R.id.image) as ImageView
            phoneHome = findViewById(R.id.phone_home) as TextView
            phoneCellphone = findViewById(R.id.phone_cellphone) as TextView
            phoneOffice = findViewById(R.id.phone_office) as TextView
            birthDay = findViewById(R.id.birth_day) as TextView
            addressHome = findViewById(R.id.address_home) as TextView
            addressWork = findViewById(R.id.address_work) as TextView
            createdAt = findViewById(R.id.created_at) as TextView
            phonesLabel = findViewById(R.id.phones_label) as TextView
            addressesLabel = findViewById(R.id.addresses_label) as TextView
            toolBar = findViewById(R.id.toolbar) as Toolbar
            loading = findViewById(R.id.progress) as ProgressBar
            coordinator = findViewById(R.id.coordinator) as CoordinatorLayout
        }

        return view
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        if (isAdded && outState != null) {
            with(outState) {
                putParcelable(CONTACT_KEY, contact)
            }
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mainActivity.setVisibilityToolbar(View.GONE)
        if (savedInstanceState != null && savedInstanceState.containsKey(CONTACT_KEY)) {
            with(savedInstanceState) {
                contact = getParcelable(CONTACT_KEY)
            }
            setupContact(contact)
        } else {
            getContact()
        }
    }

    override fun onDestroy() {
        mainActivity.restoreToolbar()
        super.onDestroy()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> mainActivity.onBackPressed()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun getContact() {
        if (connectivityManager.isNetworkAvailable()) {
            setupVisibleContent()
            contactsDao = ContactsDao().apply { contactListener = this@ContactDetailFragment }
            contactsDao.getContactById(arguments.getCharSequence(CONTACT_ARG_KEY).toString())
        } else {
            showNoConnection()
        }
    }

    private fun setupVisibleContent() {
        showLoading()
        hideNoConnection()
    }

    private fun setupToolbar(title: String) {
        toolBar.title = title
        mainActivity.setSupportActionBar(toolBar)
        mainActivity.supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    private fun setupContact(contact: Contact) {
        image.loadUrl(contact.photo, R.drawable.contact_detail_image_placeholder)
        birthDay.text = String.format(context.getString(R.string.birthday_placeholder),
                contact.birthDate.toDateWithFormat("yyyy-MM-dd", "MMM dd"))

        createdAt.text = String.format(context.getString(R.string.created_at_placeholder),
                contact.createdAt.toDateWithFormat("yyyy-MM-dd'T'HH:mm:ss.S'Z'", "MMMM yyyy"))

        processNumbers(contact.phones)
        contact.addresses?.let { processAddresses(contact.addresses) }
        setupToolbar(contact.getFullName())
        hideLoading()
        showContent()
    }

    fun processNumbers(phones: List<Phone>) {
        phones.forEach {
            when (it.type) {
                PhoneNumberType.HOME.type -> setupNumber(phoneHome, String.format(context.getString(R.string.home_phone_placeholder), it.number), it.number)
                PhoneNumberType.CELLPHONE.type -> setupNumber(phoneCellphone, String.format(context.getString(R.string.cellphone_phone_placeholder), it.number), it.number)
                PhoneNumberType.OFFICE.type -> setupNumber(phoneOffice, String.format(context.getString(R.string.office_phone_placeholder), it.number), it.number)
            }
        }
    }

    fun processAddresses(addresses: List<Address>) {
        if(addressesLabel.visibility == View.GONE) addressesLabel.visibility = View.VISIBLE

        with(addresses[0]) {
            home?.let {
                setupAddress(addressHome, String.format(context.getString(R.string.home_address_placeholder), it))
            }

            work?.let {
                setupAddress(addressWork, String.format(context.getString(R.string.work_address_placeholder), it))
            }
        }
    }

    fun setupAddress(view: TextView, address: String) {
        view.text = address
        view.visibility = View.VISIBLE
    }

    fun setupNumber(numberView: TextView, numberWithPlaceholder: String, number: String?) {
        numberView.visibility = View.GONE
        number?.let {
            if(phonesLabel.visibility == View.GONE) phonesLabel.visibility = View.VISIBLE
            numberView.text = numberWithPlaceholder
            numberView.visibility = View.VISIBLE
        }
    }

    private fun showContent() {
        coordinator.visibility = View.VISIBLE
    }

    companion object {
        fun newInstance(id: String): ContactDetailFragment {
            return ContactDetailFragment().apply {
                arguments = Bundle().apply {
                    putCharSequence(CONTACT_ARG_KEY, id)
                }
            }
        }
    }
}
