package com.ezanetta.contacts.ui.fragments

import android.content.Context
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.ezanetta.contacts.R
import com.ezanetta.contacts.ui.activities.MainActivity

abstract class BaseFragment : Fragment() {

    var baseActions: BaseActions? = null
    private lateinit var noConnection: LinearLayout
    private lateinit var retryButton: Button
    private lateinit var loading: ProgressBar
    protected val connectivityManager: ConnectivityManager
            by lazy { activity.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager }
    protected lateinit var mainActivity: MainActivity

    interface BaseActions {
        fun onRetry()
    }

    protected abstract fun getLayout(): Int

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        val view = inflater.inflate(getLayout(), container, false)
        mainActivity = activity as MainActivity

        with(view) {

            loading = findViewById(R.id.progress) as ProgressBar
            noConnection = findViewById(R.id.no_connection) as LinearLayout
            retryButton = findViewById(R.id.retry) as Button

            retryButton.setOnClickListener {
                baseActions?.onRetry()
            }
        }

        return view
    }

    fun hideLoading() {
        loading.visibility = View.GONE
    }

    fun showLoading() {
        loading.visibility = View.VISIBLE
    }

    fun showNoConnection() {
        hideLoading()
        noConnection.visibility = View.VISIBLE
    }

    fun hideNoConnection() {
        noConnection.visibility = View.GONE
    }
}