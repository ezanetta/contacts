package com.ezanetta.contacts.ui.fragments


import android.os.Bundle
import android.os.Parcelable
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.ezanetta.contacts.R
import com.ezanetta.contacts.domain.dao.ContactsDao
import com.ezanetta.contacts.domain.model.Contact
import com.ezanetta.contacts.ui.adapters.ContactsAdapter
import com.ezanetta.contacts.utils.CONTACTS_KEY
import com.ezanetta.contacts.utils.LIST_STATE_KEY
import com.ezanetta.contacts.utils.extensions.isNetworkAvailable


class ContactsFragment : BaseFragment(), ContactsDao.ContactsDaoActions, BaseFragment.BaseActions {

    private lateinit var contactsDao: ContactsDao
    private lateinit var contactsRV: RecyclerView
    private val contacts: MutableList<Contact> = ArrayList()
    private lateinit var linearLayoutManager: LinearLayoutManager
    private var listState: Parcelable? = null
    val TAG: String = ContactsFragment::class.java.simpleName

    override fun onSuccess(contacts: List<Contact>) {
        this.contacts.clear()
        this.contacts.addAll(contacts)
        setupAdapter()
    }

    override fun onError(throwable: Throwable) {
        Log.d(TAG, "Error ${throwable.localizedMessage}")
    }

    override fun onRetry() {
        fetchContacts()
    }

    override fun getLayout(): Int = R.layout.fragment_contacts

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View {

        val view = super.onCreateView(inflater, container, savedInstanceState)
        baseActions = this
        contactsDao = ContactsDao().apply { contactsListener = this@ContactsFragment }

        linearLayoutManager = LinearLayoutManager(mainActivity)

        with(view) {
            contactsRV = findViewById(R.id.contactsRV) as RecyclerView
        }

        setupNewsRecyclerView()

        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        
        if (savedInstanceState != null && savedInstanceState.containsKey(CONTACTS_KEY)) {
            with(savedInstanceState) {
                contacts.addAll(getParcelableArrayList(CONTACTS_KEY))
                listState = getParcelable(LIST_STATE_KEY)
            }
            setupAdapter()
        } else {
            fetchContacts()
        }
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)

        if (isAdded && outState != null && contacts.isNotEmpty()) {
            listState = linearLayoutManager.onSaveInstanceState()
            with(outState) {
                putParcelable(LIST_STATE_KEY, listState)
                putParcelableArrayList(CONTACTS_KEY, contacts as ArrayList<out Parcelable>)
            }
        }
    }

    private fun fetchContacts() {
        if (connectivityManager.isNetworkAvailable()) {
            setupVisibleContent()
            contactsDao.getContacts()
        } else {
            showNoConnection()
        }
    }

    private fun setupNewsRecyclerView() {
        with(contactsRV) {
            layoutManager = linearLayoutManager
            val space = activity.resources.getDimensionPixelOffset(R.dimen.small_size)
            addItemDecoration(com.ezanetta.simplenews.ui.adapters.ContactItemDecoration(space))
        }
    }

    private fun setupAdapter() {
        restorePosition()
        contactsRV.adapter = ContactsAdapter(contacts) { goToContact(it.id) }
        hideLoading()
    }

    private fun setupVisibleContent() {
        showLoading()
        hideNoConnection()
        showContacts()
    }

    private fun showContacts() {
        contactsRV.visibility = View.VISIBLE
    }

    private fun restorePosition() = linearLayoutManager.onRestoreInstanceState(listState)

    private fun goToContact(id: String) {
        mainActivity.goToFragment(ContactDetailFragment.newInstance(id), TAG)
    }

    companion object {
        fun newInstance(): ContactsFragment {
            return ContactsFragment()
        }
    }
}
