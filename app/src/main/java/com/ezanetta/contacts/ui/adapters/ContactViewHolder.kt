package com.ezanetta.contacts.ui.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.ezanetta.contacts.R
import com.ezanetta.contacts.domain.model.Contact
import com.ezanetta.contacts.domain.model.PhoneNumberType
import com.ezanetta.contacts.domain.model.Phone
import com.ezanetta.contacts.utils.extensions.loadUrl
import com.ezanetta.contacts.utils.extensions.toDateWithFormat

class ContactViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    private var image: ImageView = itemView.findViewById(R.id.image) as ImageView
    private var fullName: TextView = itemView.findViewById(R.id.full_name) as TextView
    private var phoneHome: TextView = itemView.findViewById(R.id.phone_home) as TextView
    private var phoneCellphone: TextView = itemView.findViewById(R.id.phone_cellphone) as TextView
    private var phoneOffice: TextView = itemView.findViewById(R.id.phone_office) as TextView
    private var birthDay: TextView = itemView.findViewById(R.id.birth_day) as TextView
    private var context: Context = itemView.context

    fun bindContact(contact: Contact) {
        image.loadUrl(contact.thumbnail)
        fullName.text = contact.getFullName()
        birthDay.text = String.format(context.getString(R.string.birthday_placeholder),
                contact.birthDate.toDateWithFormat("yyyy-MM-dd", "MMM dd"))
        processNumbers(contact.phones)
    }

    fun processNumbers(phones: List<Phone>) {
        phones.forEach {
            when (it.type) {
                PhoneNumberType.HOME.type -> setupNumber(phoneHome, String.format(context.getString(R.string.home_phone_placeholder), it.number), it.number)
                PhoneNumberType.CELLPHONE.type -> setupNumber(phoneCellphone, String.format(context.getString(R.string.cellphone_phone_placeholder), it.number), it.number)
                PhoneNumberType.OFFICE.type -> setupNumber(phoneOffice, String.format(context.getString(R.string.office_phone_placeholder), it.number), it.number)
            }
        }
    }

    fun setupNumber(numberView: TextView, numberWithPlaceholder: String, number: String?) {
        numberView.visibility = View.GONE
        number?.let {
            numberView.text = numberWithPlaceholder
            numberView.visibility = View.VISIBLE
        }
    }
}