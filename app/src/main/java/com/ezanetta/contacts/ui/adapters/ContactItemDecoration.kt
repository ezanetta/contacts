package com.ezanetta.simplenews.ui.adapters

import android.graphics.Rect
import android.support.v7.widget.RecyclerView
import android.view.View

class ContactItemDecoration(val space: Int) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(outRect: Rect, view: View, parent: RecyclerView,
                                state: RecyclerView.State) {

        when(parent.getChildAdapterPosition(view)) {
            0 -> setupRectTop(outRect, space)
            state.itemCount - 1 -> setupRectBottom(outRect, space)
            else -> setupRectTopAndBottom(outRect, space)
        }
    }

    fun setupRectTop(outRect: Rect, space: Int) {
        outRect.top = space * 2
        outRect.bottom = space
    }

    fun setupRectTopAndBottom(outRect: Rect, space: Int) {
        outRect.bottom = space
        outRect.top = space
    }

    fun setupRectBottom(outRect: Rect, space: Int) {
        outRect.bottom = space * 2
        outRect.top = space
    }
}