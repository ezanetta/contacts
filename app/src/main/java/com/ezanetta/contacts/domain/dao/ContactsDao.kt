package com.ezanetta.contacts.domain.dao

import com.ezanetta.contacts.domain.model.Contact
import com.ezanetta.contacts.networking.ContactsApiClient
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class ContactsDao {

    lateinit var contactsCall: Call<List<Contact>>
    lateinit var contactByIdCall: Call<Contact>
    var contactsListener: ContactsDaoActions? = null
    var contactListener: ContactDaoActions? = null

    interface ContactsDaoActions {
        fun onSuccess(contacts: List<Contact>)
        fun onError(throwable: Throwable)
    }

    interface ContactDaoActions {
        fun onSuccess(contact: Contact)
        fun onError(throwable: Throwable)
    }

    fun getContacts() {
        contactsCall = ContactsApiClient.contactsApi.getContacts().apply {
            enqueue(object : Callback<List<Contact>> {

                override fun onResponse(call: Call<List<Contact>>, response: Response<List<Contact>>) {
                    if (response.isSuccessful) {
                        contactsListener?.onSuccess(response.body()!!)
                    }
                }

                override fun onFailure(call: Call<List<Contact>>?, t: Throwable) {
                    contactsListener?.onError(t)
                }
            })
        }
    }

    fun getContactById(id: String) {
        contactByIdCall = ContactsApiClient.contactsApi.getContact(id).apply {
            enqueue(object : Callback<Contact> {

                override fun onResponse(call: Call<Contact>, response: Response<Contact>) {
                    if (response.isSuccessful) {
                        contactListener?.onSuccess(response.body()!!)
                    }
                }

                override fun onFailure(call: Call<Contact>, t: Throwable) {
                    contactListener?.onError(t)
                }
            })
        }
    }
}