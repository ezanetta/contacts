package com.ezanetta.contacts.domain.model

enum class PhoneNumberType(val type: String) {
    HOME("Home"),
    CELLPHONE("Cellphone"),
    OFFICE("Office")
}