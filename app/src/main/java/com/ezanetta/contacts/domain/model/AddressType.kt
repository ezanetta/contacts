package com.ezanetta.contacts.domain.model

enum class AddressType(val type: String) {
    HOME("home"),
    CELLPHONE("work")
}