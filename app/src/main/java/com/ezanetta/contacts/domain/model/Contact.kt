package com.ezanetta.contacts.domain.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

data class Contact(@SerializedName("user_id") val id: String,
                   @SerializedName("created_at") val createdAt: String,
                   @SerializedName("birth_date") val birthDate: String,
                   @SerializedName("first_name") val firstName: String,
                   @SerializedName("last_name") val lastName: String,
                   @SerializedName("phones") val phones: List<Phone>,
                   @SerializedName("thumb") val thumbnail: String,
                   @SerializedName("photo") val photo: String,
                   @SerializedName("addresses") val addresses: List<Address>?) : Parcelable {

    constructor(parcel: Parcel) : this(
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.readString(),
            parcel.createTypedArrayList(Phone),
            parcel.readString(),
            parcel.readString(),
            parcel.createTypedArrayList(Address))

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(id)
        parcel.writeString(createdAt)
        parcel.writeString(birthDate)
        parcel.writeString(firstName)
        parcel.writeString(lastName)
        parcel.writeTypedList(phones)
        parcel.writeString(thumbnail)
        parcel.writeString(photo)
        parcel.writeTypedList(addresses)
    }

    override fun describeContents(): Int {
        return 0
    }

    fun getFullName(): String = "$firstName $lastName"

    companion object CREATOR : Parcelable.Creator<Contact> {
        override fun createFromParcel(parcel: Parcel): Contact {
            return Contact(parcel)
        }

        override fun newArray(size: Int): Array<Contact?> {
            return arrayOfNulls(size)
        }
    }
}